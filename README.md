TIME EVOLUTION CODE FOR INITIAL EXCITED STATE OF ABSORBING METAL ATOM IN L-EDGE XAS.

Code requires initial excited wavefunction coefficients and excited Hamiltonian for absorbing metal of choice.
Input parameters of the  code include: time step, number of iterations and the index of the wavefunction/Hamiltonian.


----- Files for Ti [SrTiO3] -----
Initial wavefunction and Hamiltonian obtain from ESRF multiplet code; atomic units used through-out this code (e=m=hbar=1)


ti-multC.txt --- initial excited wavefunction coefficients (index 60) 

ti-multH.txt --- initial excited Hamiltonian (index 60 x 60)

Tiatomdata.txt --- input parameters for Tiatom.f90 code. Time step t, index size of coefficients n, number of steps nn.

Tiatom.f90 --- time evolution Fortran90 code. Initial wavefunction and Hamiltonian read in. 
Unitary time evolution matrix built from Hamiltonian and time evolves coefficients for nn iterations. 
Time evolved coefficients stored in file name 'CoefficientsTi.txt'

correlationTi.m --- Matlab code for calculating the Fourier transform of the time-correlation function for the wavefunction. 
Within the code there is a Gaussian broadening section to which the coordinates of the peaks need to be defined manually.
Atomic units converted to eV for the Energy spectrum axis. 


----- Files for Ni [NiO] -----
Initial wavefunction and Hamiltonian obtain from Hilbert++ code; Energy in eV throughout code.


Ni-multC.txt --- initial excited wavefunction coefficients (index 60) 

Ni-multH.txt --- initial excited Hamiltonian (index 60 x 60)

Niatomdata.txt --- input parameters for Niatom.f90 code. time step t, index size of coefficients n, number of steps nn.

Niatom.f90 --- time evolution Fortran90 code. Initial wavefunction and Hamiltonian read in. 
Unitary time evolution matrix built from Hamiltonian and time evolves coefficients for nn iterations. 
Time evolved coefficients stored in file name 'CoefficientsNi.txt'

correlationTi.m --- Matlab code for calculating the Fourier transform of the time-correlation function for the wavefunction.
 