
M1=dlmread('CoefficentsNi2.txt'); % read in coeffs from fortran90 program
b=60; %basis size
N=5000001; %number of time steps
Mc1=complex(M1(:,1:2:2*b),M1(:,2:2:2*b));
y1=zeros(N,1);

%Correlation function
for t=1:N
    for i=1:b
y1(t)=y1(t)+(210/N)*Mc1(1,i)*conj(Mc1(t,i)); 
    end
end

%Fourier transform
ya=abs(fftshift((fft(y1))));

%Plot correlation function
figure
t=linspace(0,N-1,N);
subplot(2,1,1)
plot(t,y1,'c');
legend('boxoff');
title('Correlation function');    
ylabel('C(t)');
xlabel('time steps');

s=dlmread('linespecTI.txt');
x=s(:,1); %shift of +806.1 applied
y=s(:,2);
s1=dlmread('broadTI.txt');
x1=s1(:,1); %shift of +806.1 applied
y1=s1(:,2);

dt=2E-20; %time step size
h=4.13566766225E-15; %h in eV
f=h*(-N/2:(N/2)-1)/(N*dt); %x-axis using nyquist theorem

%Plot energy spectrum
subplot(2,1,2)
plot(x,y,f,ya,x1,y1);
legend('boxoff');
title('FT');    
ylabel('Intensity');
xlim([815,845]);
xlabel('Energy (eV)');

%spec=fopen('speclineX.txt','w');
%fprintf(spec,'%f32 ',f(2539415:2541108));
%fclose(spec);
%spec=fopen('multHlineX.txt','w');
%fprintf(spec,'%f32 ',x);
%fclose(spec);
%spec=fopen('multHlineY.txt','w');
%fprintf(spec,'%f32 ',y);
%fclose(spec);
%spec=fopen('speclineY.txt','w');
%fprintf(spec,'%f32 ',ya(2539415:2541108));
%fclose(spec);
