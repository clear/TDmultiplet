!Time evolution of initial excited wavefunction of Ni using a Multiplet Hamiltonian from Hilbert++. UNITS IN EV.
program main
implicit none
integer(kind = kind(1.0d0))  :: a,b,i
include 'Niatomdata.txt'
complex(kind = kind(1.0d0)), dimension (n,n)::U,H
complex(kind = kind(1.0d0)),dimension(n) :: C
!INITAL COEFFICIENTS
open(unit=86,file="Ni-multC.txt",status="old")
 do i=1,n
    read(86,*)C(i)
 end do
 close(86)
open(unit=99,file="CoefficentsNi2.txt",form="formatted",status="unknown",position="append")
 write(99,5)C
 5 format(60(2x,f50.40,2x,f50.40))
close(99)
!MULTIPLET HAMLITONIAN
open(unit=87,file="Ni-multH.txt",status="old")
read(87,*)((H(a,b),a=1,n),b=1,n)
close(87)
call UNITARY(H,U)
!TIME EVOLUTION
do i=1,nn
 C=matmul(U,C)
 open(unit=99,file="CoefficentsNi2.txt",form="formatted",status="unknown",position="append")
  write(99,5)C
 close(99)
end do
end program main
 
subroutine UNITARY(H,U)
implicit none
integer(kind = kind(1.0d0)):: a,b,info
include 'Niatomdata.txt'
complex(kind = kind(1.0d0)), dimension (n,n)::Nu,X,De,Deinv,Id
complex(kind = kind(1.0d0)), dimension (n,n),intent(out)::U
complex(kind = kind(1.0d0)), dimension (n,n),intent(in)::H
complex(kind = kind(1.0d0)),parameter :: Z=(0,1)
integer(kind = kind(1.0d0)), dimension(n)::ipiv
complex(kind = kind(1.0d0)),Dimension(n*n)::work
real(kind=kind(1.0d0)),parameter::hb=6.582119514E-16
!IDENTITY MATRIX
do a=1,n
 do b=1,n
  Id(a,b)=(0.0,0.0)
 end do
  Id(a,a)=(1.0,0.0)
end do
!UNITARY MATRIX
X=Z*matmul(Id,H)
Nu=Id-X*(t/(2*hb))
De=Id+X*(t/(2*hb))
Deinv=De
call ZGETRF(n,n,Deinv,n,ipiv,info)! function for the LU matrix
call ZGETRI(n,Deinv,n,ipiv,work,n*n,info)! function for the inverse matrix
U=matmul(Nu,Deinv)
end subroutine UNITARY

