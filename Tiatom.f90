!Time evolution of initial excited wavefunction of Ni using a Multiplet Hamiltonian from Hilbert++. UNITS IN HARTREE.
program main
implicit none
integer(kind = kind(1.0d0))  :: a,b,i
real(kind = kind(1.0d0))  :: z,y
include 'Tiatomdata.txt'
complex(kind = kind(1.0d0)) , dimension (n,n)::U,H
complex(kind = kind(1.0d0)) ,dimension(n) :: C
!INITIAL COEFFICIENTS
open(unit=86,file="ti-multC.txt",status="old")
 do i=1,n
  read(86,*)z,y 
 C(i)=cmplx(z,y)
 end do
close(86)
open(unit=96,file="CoefficentsTi.txt",form="formatted",status="unknown",position="append")
 write(96,5)C
 5 format(60(2x,f33.30,2x,f33.30)) !! 60=n (basis)
close(96)
!MULTIPLET HAMLITONIAN
open(unit=87,file="ti-multH.txt",status="old")
read(87,*)((H(a,b),a=1,n),b=1,n)
close(87)
call UNITARY(H,U)
!TIME EVOLUTION
do i=1,nn
 C=matmul(U,C)
 open(unit=96,file="CoefficentsTi.txt",form="formatted",status="unknown",position="append")
  write(96,5)C
 close(96)
end do
end program main 

subroutine UNITARY(H,U)
implicit none
integer(kind = kind(1.0d0)):: a,b,info
include 'Tiatomdata.txt'
complex(kind = kind(1.0d0)), dimension (n,n)::Nu,X,De,Deinv,Id
complex(kind = kind(1.0d0)), dimension (n,n),intent(out)::U
complex(kind = kind(1.0d0)), dimension (n,n),intent(in)::H
complex(kind = kind(1.0d0)),parameter :: Z=(0,1)
integer(kind = kind(1.0d0)), dimension(n)::ipiv
complex(kind = kind(1.0d0)),Dimension(n*n)::work
!IDENTITY MATRIX
do a=1,n
 do b=1,n
  Id(a,b)=(0.0,0.0)
 end do
  Id(a,a)=(1.0,0.0)
end do
!UNITARY MATRIX
X=Z*matmul(Id,H)
Nu=Id-X*(t/2.0)
De=Id+X*(t/2.0)
Deinv=De
call ZGETRF(n,n,Deinv,n,ipiv,info)! LU matrix !
call ZGETRI(n,Deinv,n,ipiv,work,n*n,info)! Inverse matrix
U=matmul(Nu,Deinv)
end subroutine UNITARY

