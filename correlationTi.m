
M1=dlmread('CoefficentsTi.txt'); %read in coeffs from fortran90 program
b=60; %basis size
N=200001; %step size
Mc1=complex(M1(:,1:2:2*b),M1(:,2:2:2*b));
y1=zeros(N,1);

%Correlation function
for t=1:N
    for i=1:b
y1(t)=y1(t)+(1/N)*Mc1(1,i)*conj(Mc1(t,i));
    end
end

%Fourier transform
ya=abs(fftshift(fft(y1)));
dt=0.41; %time step (atomic units)
f=(2*pi)*27.2113860217*(-N/2:(N/2)-1)/(N*dt); %converted to eV

%Plot correlation function
figure
t=linspace(0,N-1,N);
subplot(2,1,1)
plot(t,y1,'c');
legend('boxoff');
title('Correlation function');    
ylabel('Re C(t)');
xlabel('time steps');

s1=dlmread('timultHspec.txt'); %time independent line spectrum read in to be broadened
x1=s1(:,1); %shifted by 460
y1=s1(:,2);
s=dlmread('linespec.txt'); %time dependent FT line spectrum read in to be broadened
x=s(:,1); %shifted by 460
y=s(:,2);

%Time independent gaussian broadening
Xo=([-3.1515022,-2.3778707,-1.3780500,-1.1100105,0.88829599,4.0825873,6.3534481]);
Yo=([1.5*0.13334923E-02,1.5*0.15034824E-02,6.5*0.96737779E-02,1.5*0.28955477E-02,1*0.87515767E-01,1.3*0.41415650E-01,0.6*0.15183351]);
yg=zeros(11511,1);
sig=([0.3,0.4,0.3,0.3,0.6,0.6,0.8]);
xG=s(:,1)-460;
for i=1:7
ys=zeros(11511,1);
for j=1:11511;
ys(j)=exp(-(xG(j)-Xo(i)).^2/(2*sig(i)^2))*Yo(i);
end
yg=yg+ys;
end
yg=yg*2;

%Time dependent gaussian broadening
Xo1=([-3.14997532,-2.37746732,-1.37873332,-1.11080532,0.88770632,4.07677632,6.33383232]);
Yo1=([1.5*0.00146832,1.5*0.00226532,5.5*0.01209832,1.5*0.00443832,1*0.08917832,1.3*0.04011132,0.6*0.15245632]);
sig1=([0.3,0.3,0.3,0.4,0.6,0.6,0.8]);
yg1=zeros(11511,1);
xG=s(:,1)-460;
for i=1:7
ys1=zeros(11511,1);
for j=1:11511;
ys1(j)=exp(-(xG(j)-Xo1(i)).^2/(2*sig1(i)^2))*Yo1(i);
end
yg1=yg1+ys1;
end
yg1=yg1*2;

%Energy spectrum
subplot(2,1,2)
plot(x,y,x1,y1,x,yg,x,yg1);
legend('boxoff');
title('FT');    
ylabel('Relative Intensity');
xlabel('Energy (eV)');
